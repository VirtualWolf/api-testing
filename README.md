## Installation
1. `npm install`
2. Set environment variables:
    - `PG_HOST`
    - `PG_USER`
    - `PG_PASSWORD`
    - `PG_DATABASE`
3. `npm start`