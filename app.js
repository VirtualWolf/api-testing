const express = require('express');
const bodyParser = require('body-parser');
const app = express();

const jsonParser = bodyParser.json();

const { Pool } = require('pg');
const pool = new Pool({
    user: process.env.PG_USER,
    host: process.env.PG_HOST,
    database: process.env.PG_DATABASE,
    password: process.env.PG_PASSWORD,
    port: 5432
});

app.get('/', (req, res) => {
    res.send('Hello world!');
});

app.get('/dogs', async (req, res) => {
    try {
        const results = await pool.query('SELECT * FROM dogs ORDER BY id');
        res.json(results.rows);
    } catch (err) {
        res.status(500).send('Internal server error');
    }
});

app.post('/dogs', jsonParser, async (req, res) => {
    if (!req.body.name || !req.body.breed) {
        res.send('Must specify both name and breed');
    }

    try {
        const result = await pool.query('INSERT INTO dogs (name, breed) VALUES ($1, $2) RETURNING id, name, breed', [req.body.name, req.body.breed]);
        res.send(result.rows[0]);
    } catch (err) {
        res.status(500).send('Internal server error');
    }
});

app.get('/dogs/:id', async (req, res) => {
    try {
        const results = await pool.query('SELECT * FROM dogs WHERE id = $1', [req.params.id]);
        res.json(results.rows[0]);
    } catch (err) {
        res.status(500).send('Internal server error');
    }
});

app.put('/dogs/:id', jsonParser, async (req, res) => {
    try {
        const result = await pool.query('UPDATE dogs SET name = COALESCE($1, name), breed = COALESCE($2, breed) WHERE id = $3 RETURNING id, name, breed', [req.body.name, req.body.breed, req.params.id]);
        res.send(result.rows[0]);
    } catch (err) {
        res.status(500).send('Internal server error');
    }
});

app.delete('/dogs/:id', jsonParser, async (req, res) => {
    try {
        const result = await pool.query('DELETE FROM dogs WHERE id = $1', [req.params.id]);
        res.send(result.command);
    } catch (err) {
        res.status(500).send('Internal server error');
    }
});

app.listen(3000, async function () {
    await pool.connect();
    await pool.query('CREATE TABLE IF NOT EXISTS dogs (id SERIAL, name TEXT NOT NULL, breed TEXT NOT NULL)');
    console.log('Listening on port 3000!');
});